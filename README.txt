Simple Cool Message module:
---------------------------

CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features 
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Simple cool message is light weight and provides a beautiful display of system messages 
(errors, warning, status etc.). These messages displaying at the top of the page using CSS3 and jQuery.
If you click on message then it will hide.

FEATURES:
---------

The Simple Cool Message module:

* Align drupal set messages as top of the page.
* Select your own Background color using configuration form for drupal set messages.
* Disable Simple Cool Message using configuration form.

BASED ON
--------

Cool notification messages with CSS3 & jQuery
by Catalin Rosu, a.k.a Red.
http://www.red-team-design.com/cool-notification-messages-with-css3-jquery.


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
   Link: https://www.drupal.org/documentation/install/modules-themes/modules-8

2. Go to "Administer" -> "Extend" and enable the Simple Cool Message module.


CONFIGURATION
-------------

 * Go to "Configuration" -> "User interface" -> "Simple Cool Message" to find all the configuration
   options.

Add a Simple cool message formate to your Drupal set messages:
----------------------------------------

Go to "Configuration" -> "User interface" -> "Simple Coll Message" and select "colors" for 
Status, Warning and Errors.


MAINTAINERS
-----------

Current maintainers:

 * Arulraj M(arulraj) - https://www.drupal.org/u/arulraj

Requires - Drupal 8
License - GPL (see LICENSE)

